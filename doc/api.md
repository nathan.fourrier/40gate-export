# Fortigate API commands

https://fndn.fortinet.net/index.php?/fortiapi/1-fortios/

## List interfaces

 /api/v2/monitor/system/interface?include_aggregate=true&include_vlan=true

## List interfaces (including virtual interfaces like tunnels)

 /api/v2/monitor/system/available-interfaces

## List policies

/api/v2/cmdb/firewall/policy?with_meta=1&datasource=1&exclude-default-values=1&start=0

## List addresses groups

/api/v2/cmdb/firewall/addrgrp

## List addresses

/api/v2/cmdb/firewall/address

## List custom services

/api/v2/cmdb/firewall.service/custom

## List services groups

/api/v2/cmdb/firewall.service/group

## List schedules

/api/v2/cmdb/firewall.schedule/recurring

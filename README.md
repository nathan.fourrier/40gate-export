# 40gate export

## Input methods

You can pass parameters if you want to run automatically the export without human action.

``` code
 -i, --p : ip address of your firewall
 -u, --user : The username of the account used for login
 -u, --user : The username of the account used for login
 -p, --pwd  : The password of the account used for login 
 -y, --yes
 -h, --help 
```

You can also leave the parameters empty and enter all credentials in the cli when requested.


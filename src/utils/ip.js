export const netmask2CIDR = (netmask) => netmask
  .split('.')
  .map(Number)
  // eslint-disable-next-line no-bitwise
  .map((part) => (part >>> 0).toString(2))
  .join('')
  .split('1').length - 1;

export default {};

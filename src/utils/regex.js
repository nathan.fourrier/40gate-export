const regex = {};
regex.singleIp = /^([0-9]{1,3}\.){3}[0-9]{1,3} 255\.255\.255\.255$/;
regex.portRange = /^[0-9]{1,5}-[0-9]{1,5}$/;
regex.portSrcDest = /.*:.*$/;
regex.singlePort = /^[0-9]{1,5}$/;

export default regex;

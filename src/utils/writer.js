import { promises as fs } from 'fs';
import logger from 'node-color-log';
import path from 'path';

export const storeExport = async (json, filename) => {
  const dirname = await path.resolve();
  fs.writeFile(`${dirname}/exports/${filename}.json`, JSON.stringify(json), 'utf8')
    .then(() => logger.info(`${filename} successfully write in${dirname}/exports/${filename}.json`))
    .catch((err) => logger.error(`Error writing file ${filename}: ${err}`));
};

export const writeJson = async (json, fileName) => {
  const dirname = await path.resolve();
  fs.writeFile(`${dirname}/out/${fileName}.json`, JSON.stringify(json, null, 4), 'utf8')
    .then(() => logger.info(`${fileName} successfully write in ${dirname}/out/${fileName}.json`))
    .catch((err) => logger.error(`Error writing file ${fileName}: ${err}`));
};

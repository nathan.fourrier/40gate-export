import logger from 'node-color-log';
import regex from './regex.js';
import { netmask2CIDR } from './ip.js';

export class Mappers {
  constructor(data) {
    this.data = data;
  }

  static policy(p) {
    return {
      name: p.name,
      interfaceIn: p.srcintf[0].name,
      interfaceOut: p.dstintf[0].name,
      src: p.srcaddr.map((addr) => ({
        datasource: addr.datasource,
        addr: addr.q_origin_key,
      })),
      dest: p.dstaddr.map((addr) => ({
        datasource: addr.datasource,
        addr: addr.q_origin_key,
      })),
      services: p.service.map((service) => ({
        name: service.q_origin_key,
        datasource: service.datasource,
      })),
      schedule: p.schedule.q_origin_key,
    };
  }

  Interface(name) {
    const iface = this.data.interfaces.find((elt) => elt.name === name);

    if (iface !== undefined) {
      if (iface.type === 'tunnel') {
        return `${name}(tunnel-vpn)`;
      }

      return name;
    }

    logger.debug(`Error searching interface ${name}, using name without mapping !!`);
    return name;
  }

  Addr(addr) {
    let list = [];
    switch (addr.datasource) {
      case 'firewall.address': {
        let ip;
        let mask;
        const result = this.data.addresses.find((entry) => entry.q_origin_key === addr.addr);
        if (result.type === 'ipmask') {
          [ip, mask] = result.subnet.split(' ');
        } else if (result.type === 'iprange') {
          const [start, end] = [result['start-ip'], result['end-ip']];
          if (start === end) {
            [ip, mask] = [start, '0.0.0.0'];
          } else {
            [ip, mask] = [`[${start},${end}]`, '255.255.255.255'];
          }
        } else {
          logger.debug(`Not found address ${addr.addr}`);
          return list;
        }

        let address;

        // remove 255.255.255.255
        if (ip === '0.0.0.0' && mask === '0.0.0.0') {
          address = 'ALL';
        } else if (mask === '255.255.255.255' || mask === '0.0.0.0') {
          address = ip;
        } else {
          address = `${ip}/${netmask2CIDR(mask)}`;
        }

        list.push(address);
        break; }
      case 'firewall.addrgrp': {
        const group = this.data.addrGrps.find((grp) => grp.q_origin_key === addr.addr);
        if (group !== undefined) {
          group.member.forEach((member) => {
            list = list.concat(
              this.Addr({ addr: member.name, datasource: 'firewall.address' }),
            );
          });
        }
        break; }
      default:
        list.push(addr.addr);
    }
    return list;
  }

  Service(service) {
    switch (service.datasource) {
      case 'firewall.service.custom': {
        const result = this.data.services.find(
          (serv) => serv.q_origin_key === service.name,
        );
        switch (result.protocol) {
          case 'TCP/UDP/SCTP': {
            const ports = [];
            ['tcp', 'udp', 'sctp'].forEach((protocol) => {
              if (result[`${protocol}-portrange`] !== '') {
                const portRanges = result[`${protocol}-portrange`].split(' ');
                portRanges.forEach((portRange) => {
                  if (regex.singlePort.test(portRange)) {
                    ports.push(`${protocol}-${portRange}`);
                  } else if (regex.portRange.test(portRange)) {
                    const [startPort, endPort] = portRange.split('-');
                    ports.push(`${protocol}-[${startPort},${endPort}]`);
                  } else if (regex.portSrcDest.test(portRange)) {
                    logger.debug(
                      'Services with src:dest ports not implemented yet !!',
                    );
                    const [destPortRange, srcPortRange] = portRange.split(':');
                    logger.debug(
                      `Port ${protocol} ${srcPortRange} to ${destPortRange}`,
                    );
                  } else {
                    logger.debug('!!!!!!UNKNOWN PATTERN!!!');
                  }
                });
              }
            });
            return ports;
          }
          case 'ICMP':
            return [`${service.name} (ICMP)`];
          case 'ICMP6':
            return [`${service.name} (ICMP6)`];
          case 'IP':
            return [`${service.name} (IP)`];
          case 'ALL':
            return [`${service.name} (ALL)`];
          default:
            logger.debug(`Unknown protocol for ${service.name}`);
            return [service.name];
        } }
      case 'firewall.service.group': {
        const group = this.data.servGrps.find(
          (grp) => grp.q_origin_key === service.name,
        );

        if (group !== undefined) {
          return group.member.reduce(
            (acc, serv) => acc.concat(
              this.Service({
                ...serv,
                datasource: 'firewall.service.custom',
              }),
            ),
            [],
          );
        }
        return [service]; }

      default:
        return [service];
    }
  }
}

export default {};

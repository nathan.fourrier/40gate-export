import logger from 'node-color-log';
import readline from 'readline-sync';
import minimist from 'minimist';
import { getPolicies } from './data/index.js';
import { writer } from './utils/index.js';
import { fetchData } from './data/fetch.js';

let pass = false;
const credentials = {};
const args = minimist(process.argv.slice(2));

if (args.h || args.help) {
  logger.info('-i, --ip  --> The ip address of the firewall ');
  logger.info('-u, --user  --> The username of the account used for login ');
  logger.info('-p, --pwd  --> The password of the account used for login ');
  logger.info('-y, --yes');
} else {
  do {
    credentials.ip = args.ip ? args.ip : readline.question('Whats the ip address of your firewall ?\n');
    credentials.user = args.user ? args.user : readline.question('Whats the username to use for login ?\n');
    credentials.pwd = args.pwd ? args.pwd : readline.question('Whats the password to use ?\n', { hideEchoBack: true });

    logger.info({ ...credentials, pwd: credentials.pwd.replaceAll(/./g, '*') });
    pass = args.y || args.yes ? true : readline.keyInYN('Try export with following credentials ?');
  } while (!pass);

  (async () => {
    const data = await fetchData(
      `https://${credentials.ip}`,
      credentials.user,
      credentials.pwd,
    );

    const policies = getPolicies(data);

    await writer.writeJson(policies, 'policies');
  })();
}

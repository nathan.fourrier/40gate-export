import logger from 'node-color-log';
import axios from 'axios';
import { CookieJar } from 'tough-cookie';
import { HttpsCookieAgent } from 'http-cookie-agent/http';

const serializeObj = (obj) => Object.entries(obj)
  .map((keyValue) => keyValue.join('='))
  .join('&');

export class FortiAPI {
  constructor(baseURL) {
    this.axios = axios.create({
      baseURL,
      withCredentials: true,
      httpsAgent: new HttpsCookieAgent({
        cookies: { jar: new CookieJar() },
        keepAlive: true,
        rejectUnauthorized: false,
      }),
    });
  }

  login(username, secretkey) {
    return this.axios.post('/logincheck', serializeObj({ username, secretkey }))
      .then((req) => logger.debug(req.data))
      .then(() => logger.info('Login success!'))
      .catch((err) => logger.error(`Error during login: ${err}`));
  }

  logout() {
    return this.axios.post('/logout')
      .then(() => logger.info('Logout success!'))
      .catch((err) => logger.error(`Error during logout: ${err}`));
  }

  fetch(url) {
    logger.debug(`Fetching url ${url}`);
    return this.axios.get(url)
      .then((res) => {
        logger.debug(res.status);
        return res;
      })
      .then((res) => res.data.results)
      .catch((err) => logger.error(err.response.data));
  }

  fetchInterfaces() {
    return this.fetch('/api/v2/monitor/system/available-interfaces');
  }

  fetchAddr() {
    return this.fetch('/api/v2/cmdb/firewall/address');
  }

  fetchAddrGrps() {
    return this.fetch('/api/v2/cmdb/firewall/addrgrp');
  }

  fetchCustomServices() {
    return this.fetch('/api/v2/cmdb/firewall.service/custom');
  }

  fetchServicesGrps() {
    return this.fetch('/api/v2/cmdb/firewall.service/group');
  }

  fetchSchedules() {
    return this.fetch('/api/v2/cmdb/firewall.schedule/recurring');
  }

  fetchPolicies() {
    return this.fetch('/api/v2/cmdb/firewall/policy?with_meta=1&datasource=1&exclude-default-values=1start=0');
  }
}

export const fetchData = async (url, username, pwd) => {
  const api = new FortiAPI(url);

  await api.login(username, pwd);

  const data = {};

  await Promise.all(
    [
      api.fetchAddr()
        .then((json) => { data.addresses = json; }),

      api.fetchAddrGrps()
        .then((json) => { data.addrGrps = json; }),

      api.fetchInterfaces()
        .then((json) => { data.interfaces = json; }),

      api.fetchPolicies()
        .then((json) => { data.policies = json; }),

      api.fetchSchedules()
        .then((json) => { data.schedules = json; }),

      api.fetchServicesGrps()
        .then((json) => { data.servGrps = json; }),

      api.fetchCustomServices()
        .then((json) => { data.services = json; }),

    ],
  )
    .catch((err) => {
      logger.error(err.message);
      process.exit();
    });

  await api.logout();

  return data;
};

import { Mappers } from '../utils/index.js';

export const getPolicies = (data) => {
  const mappers = new Mappers(data);
  // Get only the array of policies
  let { policies } = data;

  // Formatting
  policies = policies
    .map(Mappers.policy)
    // Map interfaces names
    .map((p) => ({
      ...p,
      interfaceIn: mappers.Interface(p.interfaceIn),
      interfaceOut: mappers.Interface(p.interfaceOut),
    }))
    // Map real addresses
    .map((p) => ({
      ...p,
      src: p.src.reduce((acc, addr) => acc.concat(mappers.Addr(addr)), []),
      dest: p.dest.reduce((acc, addr) => acc.concat(mappers.Addr(addr)), []),
    }))
    // Map services ports
    .map((p) => ({
      ...p,
      services: p.services.reduce(
        (acc, service) => acc.concat(mappers.Service(service)),
        [],
      ),
    }));

  return policies;
};

export default {};
